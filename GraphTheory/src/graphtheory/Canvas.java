package graphtheory;

/**
 *
 * @author mk
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Vector;

public class Canvas {

    public JFrame frame;
    private JMenuBar menuBar;
    private JButton button = new JButton("Edge Connectivity"); 
	 
    private CanvasPane canvas;
    private Graphics2D graphic;
    private Color backgroundColour;
    private Image canvasImage,  canvasImage2;
    private int selectedTool;
    private int selectedWindow;
    private Dimension screenSize;
    public int width,  height;
    private int clickedVertexIndex;
    private int clickedEdgeIndex;
    private FileManager fileManager = new FileManager();

    /////////////
    private Vector<Vertex> vertexList;
    private Vector<Edge> edgeList;
    private GraphProperties gP = new GraphProperties();
    /////////////

    public Canvas(String title, int width, int height, Color bgColour) {
        frame = new JFrame();
        frame.setTitle(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        canvas = new CanvasPane();
        InputListener inputListener = new InputListener();
        canvas.addMouseListener(inputListener);
        canvas.addMouseMotionListener(inputListener);
        frame.setContentPane(canvas);

        this.width = width;
        this.height = height;
        canvas.setPreferredSize(new Dimension(width, height));

        //events
        menuBar = new JMenuBar();
        JMenu menuOptions = new JMenu("Tools");
        JMenu menuOptions1 = new JMenu("File");
        JMenu menuOptions2 = new JMenu("Extras");
        JMenu menuOptions3 = new JMenu("Window");
        
        JMenu propertiesSubmenu = new JMenu("Properties");
        
        JMenu centralitySubmenu = new JMenu("Centrality Measures");
        
        JMenuItem item = new JMenuItem("Add Vertex");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK));
        item.addActionListener(new MenuListener());
        menuOptions.add(item);
        item = new JMenuItem("Open File");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
        item.addActionListener(new MenuListener());
        menuOptions1.add(item);
        item = new JMenuItem("Save to File");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
        item.addActionListener(new MenuListener());
        menuOptions1.add(item);
        item = new JMenuItem("Add Edges");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));
        item.addActionListener(new MenuListener());
        menuOptions.add(item);
        item = new JMenuItem("Grab Tool");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_DOWN_MASK));
        item.addActionListener(new MenuListener());
        menuOptions.add(item);
        item = new JMenuItem("Remove Tool");
        item.addActionListener(new MenuListener());
        item.setEnabled(false);
        menuOptions.add(item);
        item = new JMenuItem("Auto Arrange Vertices");
        item.addActionListener(new MenuListener());

        menuOptions2.add(item);
        item = new JMenuItem("Remove All");
        item.addActionListener(new MenuListener());
        menuOptions2.add(item);

        item = new JMenuItem("Graph");
        item.addActionListener(new MenuListener());
        menuOptions3.add(item);
        
        item = new JMenuItem("Matrices");
        item.addActionListener(new MenuListener());
        propertiesSubmenu.add(item);
        
        item = new JMenuItem("Graph Connectivity");
        item.addActionListener(new MenuListener());
        propertiesSubmenu.add(item);
        
        item = new JMenuItem("Maximal Clique");
        item.addActionListener(new MenuListener());
        propertiesSubmenu.add(item);
        
        item = new JMenuItem("Degree Centrality");
        item.addActionListener(new MenuListener());
        centralitySubmenu.add(item);
        
        item = new JMenuItem("Betweenness Centrality");
        item.addActionListener(new MenuListener());
        centralitySubmenu.add(item);
        
        item = new JMenuItem("Closeness Centrality");
        item.addActionListener(new MenuListener());
        centralitySubmenu.add(item);
        
        propertiesSubmenu.add(centralitySubmenu);
        
        menuOptions3.add(propertiesSubmenu);
        menuBar.add(menuOptions1);
        menuBar.add(menuOptions);
        menuBar.add(menuOptions2);
        menuBar.add(menuOptions3);

        frame.setJMenuBar(menuBar);
        
        button.setBounds(width/2,height/2, 20, 10);
        
//        button.setVisible(false);
        
        frame.add(button);
        
        backgroundColour = bgColour;

        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setBounds(screenSize.width / 2 - width / 2, screenSize.height / 2 - height / 2, width, height);
        frame.pack();
        setVisible(true);

        vertexList = new Vector<Vertex>();
        edgeList = new Vector<Edge>();

    }

    class InputListener implements MouseListener, MouseMotionListener {

        @Override
        public void mouseClicked(MouseEvent e) {

            if (selectedWindow == 0) {
                switch (selectedTool) {
                    case 1: {
                        Vertex v = new Vertex("" + vertexList.size(), e.getX(), e.getY());
                        vertexList.add(v);
                        v.draw(graphic);
                        break;
                    }
                    case 4: {

                        /* for (Vertex v : vertexList) {
                        if (v.hasIntersection(e.getX(), e.getY())) {
                        {
                        for (Edge d : edgeList) {
                        if (d.vertex1 == v || d.vertex2 == v) {
                        edgeList.remove(d);
                        }
                        }
                        for (Vertex x : vertexList) {
                        if (x.connectedToVertex(v)) {
                        x.connectedVertices.remove(v);
                        }
                        }
                        vertexList.remove(v);
                        }
                        }
                        }*/ break;
                    }
                }
            //refresh();
            }


        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (selectedWindow == 0 && vertexList.size() > 0) {
                switch (selectedTool) {
                    case 2: {
                        for (Vertex v : vertexList) {
                            if (v.hasIntersection(e.getX(), e.getY())) {
                                v.wasClicked = true;
                                clickedVertexIndex = vertexList.indexOf(v);
                            } else {
                                v.wasClicked = false;
                            }
                        }
                        break;
                    }
                    case 3: {

                        for (Edge d : edgeList) {
                            if (d.hasIntersection(e.getX(), e.getY())) {
                                d.wasClicked = true;
                                clickedEdgeIndex = edgeList.indexOf(d);
                            } else {
                                d.wasClicked = false;
                            }
                        }
                        for (Vertex v : vertexList) {
                            if (v.hasIntersection(e.getX(), e.getY())) {
                                v.wasClicked = true;
                                clickedVertexIndex = vertexList.indexOf(v);
                            } else {
                                v.wasClicked = false;
                            }
                        }
                        break;
                    }
                }
            }

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (selectedWindow == 0 && vertexList.size() > 0) {
                switch (selectedTool) {
                    case 2: {
                        Vertex parentV = vertexList.get(clickedVertexIndex);
                        for (Vertex v : vertexList) {
                            if (v.hasIntersection(e.getX(), e.getY()) && v != parentV && !v.connectedToVertex(parentV)) {              //System.out.println(clickedVertexIndex+" "+vertexList.indexOf(v));
                                Edge edge = new Edge(v, parentV);
                                v.addVertex(parentV);
                                parentV.addVertex(v);
                                v.wasClicked = false;
                                parentV.wasClicked = false;
                                edgeList.add(edge);
                            } else {
                                v.wasClicked = false;
                            }
                        }
                        break;
                    }
                    case 3: {
                        vertexList.get(clickedVertexIndex).wasClicked = false;
                        break;
                    }
                }
            }
            erase();
            refresh();
        }

        @Override
        public void mouseDragged(MouseEvent e) {

            if (selectedWindow == 0 && vertexList.size() > 0) {
                erase();
                switch (selectedTool) {
                    case 2: {
                        graphic.setColor(Color.RED);
                        drawLine(vertexList.get(clickedVertexIndex).location.x, vertexList.get(clickedVertexIndex).location.y, e.getX(), e.getY());
                        break;

                    }
                    case 3: {
                        if (vertexList.get(clickedVertexIndex).wasClicked) {
                            vertexList.get(clickedVertexIndex).location.x = e.getX();
                            vertexList.get(clickedVertexIndex).location.y = e.getY();
                        }
                        break;
                    }
                }
                refresh();
            }

        }

        @Override
        public void mouseMoved(MouseEvent e) {
            if (selectedWindow == 0) {
                for (Edge d : edgeList) {
                    if (d.hasIntersection(e.getX(), e.getY())) {
                        d.wasFocused = true;
                    } else {
                        d.wasFocused = false;
                    }
                }
                for (Vertex v : vertexList) {
                    if (v.hasIntersection(e.getX(), e.getY())) {
                        v.wasFocused = true;
                    } else {
                        v.wasFocused = false;
                    }
                }
                refresh();
            }

        }
    }

    class MenuListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            if (command.equals("Add Vertex")) {
                selectedTool = 1;
            } else if (command.equals("Add Edges")) {
                selectedTool = 2;
            } else if (command.equals("Grab Tool")) {
                selectedTool = 3;
            } else if (command.equals("Remove Tool")) {
                selectedTool = 4;
            } else if (command.equals("Auto Arrange Vertices")) {
                arrangeVertices();
                erase();
            } else if (command.equals("Remove All")) {
                edgeList.removeAllElements();
                vertexList.removeAllElements();
                clickedVertexIndex = 0;
                erase();
            } else if (command.equals("Open File")) {
                int returnValue = fileManager.jF.showOpenDialog(frame);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    loadFile(fileManager.loadFile(fileManager.jF.getSelectedFile()));
                    System.out.println(fileManager.jF.getSelectedFile());
                    selectedWindow=0;
                }
            } else if (command.equals("Save to File")) {
                int returnValue = fileManager.jF.showSaveDialog(frame);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    fileManager.saveFile(vertexList,fileManager.jF.getSelectedFile());
                    System.out.println(fileManager.jF.getSelectedFile());
                }
            } else if (command.equals("Graph")) {
                selectedWindow = 0;
                erase();
            } else if (command.equals("Matrices")) {
            	System.out.println("Vertex list size: " + vertexList.size());
            	System.out.println("Edge list size: " + edgeList.size());
            	gP = new GraphProperties(vertexList, edgeList);
                selectedWindow = 1;
                if (vertexList.size() > 0) {
                    //distance
                    gP.generateDistanceMatrix(vertexList);

                    //VD paths
                    gP.displayContainers();
//                    gP.maximumMatching(vertexList, edgeList);

//                    System.out.println("Edge connectivity: " + minCutEdgeSet.size());
                //gP.drawNWideDiameter();
                }
                erase();
            } else if (command.equals("Graph Connectivity")){
            	selectedWindow = 2;
            	 if (vertexList.size() > 0) {
            		 gP = new GraphProperties(vertexList, edgeList);
                     //adjacency list
//            		 int[][] matrix = gP.generateAdjacencyMatrix(vertexList, edgeList);

                     //connectivity
                     Vector<Vertex> tempList = gP.minCutVertexSet;
                     for (Vertex v : tempList) {
                         vertexList.get(vertexList.indexOf(v)).wasClicked = true;
                     }
//                     reloadVertexConnections(matrix, vertexList);
//                     gP.displayContainers(vertexList);
//                     gP.maximumMatching(vertexList, edgeList);
                     Vector<Edge> minCutEdgeSet = gP.minCutEdgeSet;
                     for(Edge edge : minCutEdgeSet){
                    	 edgeList.get(edgeList.indexOf(edge)).edgeCut = true;
                     }
                 //gP.drawNWideDiameter();
                 }
            	erase();
            } else if (command.equals("Degree Centrality")){
            	selectedWindow = 3;
            	erase();
            } else if (command.equals("Betweenness Centrality")){
            	selectedWindow = 4;
            	erase();
            }else if (command.equals("Closeness Centrality")){
            	selectedWindow = 5;
            	erase();
            } else if(command.equals("Maximal Clique")){
            	selectedWindow = 6;
            	erase();
            }
//            button.setVisible(false);
            refresh();
        }
    }

    private void arrangeVertices() {
        double deg2rad = Math.PI / 180;
        double radius = height / 5;
        double centerX = width / 2;
        double centerY = height / 2;
        int interval = 360 / vertexList.size();


        for (int i = 0; i < vertexList.size(); i++) {
            double degInRad = i * deg2rad * interval;
            double x = centerX + (Math.cos(degInRad) * radius);
            double y = centerY + (Math.sin(degInRad) * radius);
            int X = (int) x;
            int Y = (int) y;
            vertexList.get(i).location.x = X;
            vertexList.get(i).location.y = Y;
        }

    }

    private void loadFile(Vector<Vector> File) {
        vertexList = File.firstElement();
        edgeList = File.lastElement();
        erase();
    }

    public void refresh() {
        for (Edge e : edgeList) {
            e.draw(graphic);
        }
        for (Vertex v : vertexList) {
            v.draw(graphic);
        }

        canvas.repaint();
    }

    public void setVisible(boolean visible) {
        if (graphic == null) {
            Dimension size = canvas.getSize();
            canvasImage = canvas.createImage(size.width, size.height);
            canvasImage2 = canvas.createImage(size.width, size.height);
            graphic = (Graphics2D) canvasImage.getGraphics();
            graphic.setColor(backgroundColour);
            graphic.fillRect(0, 0, size.width, size.height);
            graphic.setColor(Color.black);
        }
        frame.setVisible(visible);
    }

    public boolean isVisible() {
        return frame.isVisible();
    }

    public void erase() {
        graphic.clearRect(0, 0, width, height);
    }

    public void erase(int x, int y, int x1, int y2) {
        graphic.clearRect(x, y, x1, y2);
    }

    public void drawString(String text, int x, int y, float size) {
        Font orig = graphic.getFont();
        graphic.setFont(graphic.getFont().deriveFont(1, size));
        graphic.drawString(text, x, y);
        graphic.setFont(orig);
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        graphic.drawLine(x1, y1, x2, y2);
    }
    
    public void resetColors(){
    	for (Edge e : edgeList) {
        	e.clique = false;
        	e.edgeCut = false;
//        	e.draw(graphic);
        }
        for (Vertex v : vertexList) {
            v.cliqueColor = false;
            v.wasClicked = false;
            v.wasFocused = false;

//        	v.draw(graphic);
        }
    }

    private class CanvasPane extends JPanel {

        public void paint(Graphics g) {
        	switch (selectedWindow) {
                case 0: {   //graph window
                	resetColors();
                	button.setVisible(false);
                    graphic.drawString("Vertex Count=" + vertexList.size() +
                            "  Edge Count=" + edgeList.size() +
                            "  Selected Tool=" + selectedTool, 50, height / 2 + (height * 2) / 5);
                    g.drawImage(canvasImage, 0, 0, null); //layer 1
                    g.setColor(Color.black);
                    break;
                }
                case 1: {   //matrices window
                	button.setVisible(false);
                    canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                    gP.drawAdjacencyMatrix(canvasImage2.getGraphics(), vertexList, width / 2 + 50, 50);//draw adjacency matrix
                    gP.drawDistanceMatrix(canvasImage2.getGraphics(), vertexList, width / 2 + 50, height / 2 + 50);//draw distance matrix
                    g.drawImage(canvasImage2, 0, 0, null); //layer 1

//                    g.drawString("See output console for Diameter of Graph", 100, height / 2 + 50);
                    g.drawImage(canvasImage.getScaledInstance(width / 2, height / 2, Image.SCALE_SMOOTH), 0, 0, null); //layer 1
                    g.draw3DRect(0, 0, width / 2, height / 2, true);
                    g.setColor(Color.black);

                    break;
                }
                case 2:{ //Graph properties window
//                	button.setVisible(true);
                	canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                	//connectivity
                	boolean graphConnected = gP.graphConnected;
                	boolean graphBipartite = gP.graphBipartite;
//                	int vConnectivity = gP.vertexConnectivity(vertexList).size();
//                  int eConnectivity = gP.edgeConnectivity(vertexList, edgeList).size();
                	int componentCount = gP.componentCount;
                    g.drawString("Graph connectivity: " + graphConnected, 5, height/2 + 20);
                    g.drawString("Number of Components: " + componentCount, 5, height/2 + 40);
                    g.drawString("Bipartite graph: " + graphBipartite, 5, height/2 + 60);
//                    g.drawString("Vertex connectivity: " + vConnectivity, 5, height/2 + 30);
//                    g.drawString("Edge connectivity: " + eConnectivity, 5, height/2 + 40);
                    drawString("Graph disconnects when nodes in color red are removed.", 100, height - 35, 20);
                    drawString("Graph disconnects when edges in color green are removed.", 100, height - 10, 20);
                	g.drawImage(canvasImage.getScaledInstance(width / 2, height / 2, Image.SCALE_SMOOTH), 0, 0, null); //layer 1
                    g.draw3DRect(0, 0, width / 2, height / 2, true);
                    g.setColor(Color.black);
                    break;
                }
                case 3:{ //Degree Centrality
                	resetColors();
                	canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                	g.drawImage(canvasImage, 0, 0, null); //layer 1
                	for(int i = 0; i < gP.degreeCentralityArr.length; i++){
                		int posX = gP.vList.elementAt(i).location.x;
                		int posY = gP.vList.elementAt(i).location.y;		
                		drawString("C_d = " + Integer.toString(gP.degreeCentralityArr[i]), posX - 15, posY - 25, 10);
                	}
                	break;
                } case 4:{ //Betwenness centrality
                	resetColors();
                	canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                	g.drawImage(canvasImage, 0, 0, null); //layer 1
                	for(int i = 0; i < gP.betweennessCentralityArr.length; i++){
                		int posX = gP.vList.elementAt(i).location.x;
                		int posY = gP.vList.elementAt(i).location.y;		
                		drawString("C_b = " + Double.toString(gP.betweennessCentralityArr[i]), posX - 15, posY - 25, 10);
                	}
                	break;
                }
                case 5:{ //Closeness centrality
                	resetColors();
                	canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                	g.drawImage(canvasImage, 0, 0, null); //layer 1
                	for(int i = 0; i < gP.degreeCentralityArr.length; i++){
                		int posX = gP.vList.elementAt(i).location.x;
                		int posY = gP.vList.elementAt(i).location.y;		
                		drawString("C_c = " + Double.toString(gP.closenessCentralityArr[i]), posX - 15, posY - 25, 10);
                	}
                	break;
                }
                case 6:{ //Maximal clique
                	resetColors();
                	for(Vertex v : gP.maximalCliques.get(0)){
                		v.cliqueColor = true;
                	}
                	for(Edge e : gP.eList){
                		if(gP.maximalCliques.get(0).contains(e.vertex1) && gP.maximalCliques.get(0).contains(e.vertex2)){
                			e.clique = true;
                		}
                	}
                	canvasImage2.getGraphics().clearRect(0, 0, width, height); //clear
                	g.drawImage(canvasImage, 0, 0, null); //layer 1
                }
            }

        }
    }
}

