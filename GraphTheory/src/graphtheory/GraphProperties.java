/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graphtheory;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.Vector;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

/**
 *
 * @author mk
 */
public class GraphProperties {

	public int[][] adjacencyMatrix;
	public int[][] distanceMatrix;
	public Vector<VertexPair> vpList = new Vector<VertexPair>();
	public boolean graphConnected = true;
	public boolean graphBipartite;
	public int componentCount;
	public Vector<Vertex> vList = new Vector<Vertex>();
	public Vector<Edge> eList = new Vector<Edge>();
	public Vector<Edge> minCutEdgeSet = new Vector<Edge>();
	public Vector<Vertex> minCutVertexSet = new Vector<Vertex>();
	public int[] degreeCentralityArr;
	public double[] betweennessCentralityArr;
	public double[] closenessCentralityArr;
	public ArrayList<Vector<Vertex>> maximalCliques = new ArrayList<Vector<Vertex>>();
	public boolean graphCyclic;
	public ArrayList<Vector<Vertex>> graphCycle = new ArrayList<Vector<Vertex>>();

	public GraphProperties() {

	}

	private void reloadVertexConnections() {
		for (Vertex v : this.vList) {
			v.connectedVertices.clear();
		}

		for (int i = 0; i < adjacencyMatrix.length; i++) {
			for (int j = 0; j < adjacencyMatrix.length; j++) {
				if (adjacencyMatrix[i][j] == 1) {
					vList.get(i).addVertex(vList.get(j));
				}
			}
		}
	}

	public GraphProperties(Vector<Vertex> vertexList, Vector<Edge> edgeList) {
		System.out.println("New object created");
		this.vList.setSize(vertexList.size());
		Collections.copy(this.vList, vertexList);
		this.eList.setSize(edgeList.size());
		Collections.copy(this.eList, edgeList);
		this.generateAdjacencyMatrix();
		this.graphConnected = this.isGraphConnected(this.vList);
		this.componentCount = this.countComponents();
		// can only do this if graph is connected
		this.findMaximalClique();
		this.getDegreeCentrality();
		this.getBetweennessCentrality();
		this.getClosenessCentrality();
		this.minCutEdgeSet = this.edgeConnectivity();
		this.reloadVertexConnections();
		this.minCutVertexSet = this.vertexConnectivity();
		this.reloadVertexConnections();
		this.graphBipartite = this.isBipartite();
		this.graphCyclic = this.isGraphCyclic(this.vList);
		// this.displayContainers();
//		System.out.println("Edge conn: " + this.minCutEdgeSet.size() + "\nVertex Conn: " + this.minCutVertexSet.size()
//				+ "\nGraph connected: " + this.graphConnected + "\nNumber of components: " + this.componentCount);
	}
	//1. The current vertex must be marked as visited
	//2. Recursion for all vertices adjacent to the vertex marked as visited in 1
	//3. If an adjacent is not yet visited, then recur for that adjacent vertex
	//4. If an adjacent is visited and not the parent of current vertex, then it is cycle.
	//5. Return true if graph contains cycle, else false.
	//6. Mark all the vertices that has been visited and part f the queue
	//7. Call the recursive function to detect cycle in different BFS graph
	public boolean isGraphCyclic(Vector<Vertex> vList) {
		return graphCyclic(vList);
	}

	private boolean graphCyclic(Vector<Vertex> vertexList) {
		Vector<Vertex> visitedList = new Vector<Vertex>();
	
		return (visitedList.size() == vertexList.size());
	}

	private void recurseGraphCyclic(Vector<Vertex> vertexList, Vector<Vertex> visitedList) {
		for (Vertex v : vertexList) {
			if (!visitedList.contains(v)) {
				visitedList.add(v);
				recurseGraphCyclic(v.connectedVertices, visitedList);
			}
		}
	}

	public void getDegreeCentrality() {
		degreeCentralityArr = new int[this.vList.size()];
		for (int i = 0; i < this.vList.size(); i++) {
			degreeCentralityArr[i] = this.vList.elementAt(i).getDegree();
		}
	}

	public void getBetweennessCentrality() {
		betweennessCentralityArr = new double[this.vList.size()];
		Hashtable<VertexPair, Vector<Vector<Vertex>>> shortestPaths = this.generateVertexPairPaths();
//		System.out.println(shortestPaths.size());
		for (int i = 0; i < this.vList.size(); i++) {
//			System.out.println("Currently at Vertex " + vList.get(i).name);
			Set<VertexPair> keys = shortestPaths.keySet();
			double frac = 0.0;
			for (VertexPair vp : keys) {
				if (vp.vertex1.name != vList.get(i).name && vp.vertex2.name != vList.get(i).name) {
//					System.out.println("VP: " + vp.vertex1.name + ", " + vp.vertex2.name);
					double num = 0.0;
					double denom = shortestPaths.get(vp).size();
					for (Vector<Vertex> shortestPath : shortestPaths.get(vp)) {
						System.out.println();
						if (shortestPath.contains(vList.get(i)))
							num++;
					}
					frac += num / denom;
				}
			}
			// System.out.println("Num: " + num + ", Denom: " + denom);
			betweennessCentralityArr[i] = Math.round(frac * 100D) / 100D;
			;
		}
	}

	private Hashtable<VertexPair, Vector<Vector<Vertex>>> generateVertexPairPaths() {
		Hashtable<VertexPair, Vector<Vector<Vertex>>> shortestPaths = new Hashtable<VertexPair, Vector<Vector<Vertex>>>();
		for (int a = 0; a < vList.size(); a++) { // assign vertex pairs
			for (int b = a + 1; b < vList.size(); b++) {
				VertexPair vp = new VertexPair(vList.get(a), vList.get(b));
				vpList.add(vp);
				vp.generateVertexDisjointPaths();
				int shortestDistance = vp.getShortestDistance();
				Vector<Vector<Vertex>> sP = new Vector<Vector<Vertex>>();
				for (int i = 0; i < vp.VertexDisjointContainer.size(); i++) {// for
																				// every
																				// container
																				// of
																				// the
																				// vertex
																				// pair
					for (int j = 0; j < vp.VertexDisjointContainer.get(i).size(); j++) { // for
																							// every
																							// path
																							// in
																							// the
																							// container
//						System.out.println("Path " + j + " of length " + vp.VertexDisjointContainer.get(i).get(j).size());
						if (vp.VertexDisjointContainer.get(i).get(j).size() == shortestDistance + 1) {
							// System.out.println("Putting");
							sP.add(vp.VertexDisjointContainer.get(i).get(j));
						}
					}
				}
				shortestPaths.put(vp, sP);
			}
		}
		return shortestPaths;
	}

	public void getClosenessCentrality() {
		closenessCentralityArr = new double[this.vList.size()];
		for (int i = 0; i < this.vList.size(); i++) {
			double currShortestPathSum = 0.0;
			for (Vertex other : this.vList) {
				if (this.vList.elementAt(i).name != other.name) {
					VertexPair currVP = new VertexPair(this.vList.elementAt(i), other);
					currShortestPathSum += currVP.getShortestDistance();
				}
			}
			closenessCentralityArr[i] = Math.round(((this.vList.size() - 1) / currShortestPathSum) * 100D) / 100D;
		}
	}

	//Source: https://www.geeksforgeeks.org/bipartite-graph/
	private boolean isBipartiteUtil(int G[][], int src, int colorArr[]) {
		colorArr[src] = 1;

		// Create a queue (FIFO) of vertex numbers and
		// enqueue source vertex for BFS traversal
		LinkedList<Integer> q = new LinkedList<Integer>();
		q.add(src);

		// Run while there are vertices in queue
		// (Similar to BFS)
		while (!q.isEmpty()) {
			// Dequeue a vertex from queue
			// ( Refer http://goo.gl/35oz8 )
			int u = q.getFirst();
			q.pop();

			// Return false if there is a self-loop
			if (G[u][u] == 1)
				return false;

			// Find all non-colored adjacent vertices
			for (int v = 0; v < vList.size(); ++v) {
				// An edge from u to v exists and
				// destination v is not colored
				if (G[u][v] == 1 && colorArr[v] == -1) {
					// Assign alternate color to this
					// adjacent v of u
					colorArr[v] = 1 - colorArr[u];
					q.push(v);
				}

				// An edge from u to v exists and
				// destination v is colored with same
				// color as u
				else if (G[u][v] == 1 && colorArr[v] == colorArr[u])
					return false;
			}
		}

		// If we reach here, then all adjacent vertices
		// can be colored with alternate color
		return true;
	}

	// Returns true if G[][] is Bipartite, else false
	public boolean isBipartite() {
		// Create a color array to store colors assigned
		// to all vertices. Vertex/ number is used as
		// index in this array. The value '-1' of
		// colorArr[i] is used to indicate that no color
		// is assigned to vertex 'i'. The value 1 is used
		// to indicate first color is assigned and value
		// 0 indicates second color is assigned.
		int colorArr[] = new int[vList.size()];
		for (int i = 0; i < vList.size(); ++i)
			colorArr[i] = -1;

		// This code is to handle disconnected graph
		for (int i = 0; i < vList.size(); i++) {
			if (colorArr[i] == -1) {
				if (!isBipartiteUtil(this.adjacencyMatrix, i, colorArr)) return false;
			}
		}
		return true;
	}

	public void generateAdjacencyMatrix() {
		this.adjacencyMatrix = new int[vList.size()][vList.size()];

		for (int a = 0; a < vList.size(); a++)// initialize
		{
			for (int b = 0; b < vList.size(); b++) {
				adjacencyMatrix[a][b] = 0;
			}
		}

		for (int i = 0; i < eList.size(); i++) {
			adjacencyMatrix[vList.indexOf(eList.get(i).vertex1)][vList.indexOf(eList.get(i).vertex2)] = 1;
			adjacencyMatrix[vList.indexOf(eList.get(i).vertex2)][vList.indexOf(eList.get(i).vertex1)] = 1;
		}
	}

	/**
	 * 
	 * @param vList
	 */
	public int countComponents() {
		Vector<Vertex> origList = new Vector<Vertex>();
		Vector<Vertex> toBeRemoved = new Vector<Vertex>();
		Vector<Vertex> representativeList = new Vector<Vertex>();

		origList.setSize(vList.size());
		Collections.copy(origList, vList);

		for (Vertex curr : origList) {
			// System.out.println("curr: " + curr.name);
			if (!toBeRemoved.contains(curr)) {
				// System.out.println(curr.name + "'s degree: " +
				// curr.getDegree());
				if (curr.getDegree() > 0) {
					for (Vertex other : origList) {
						if (curr.name != other.name && !toBeRemoved.contains(other)) {
							VertexPair currVP = new VertexPair(curr, other);
							// System.out.println("Distance between " +
							// curr.name + " and " + other.name + ": " +
							// currVP.getShortestDistance());
							if (currVP.getShortestDistance() != -1)
								toBeRemoved.add(other);
						}
					}
					representativeList.add(curr);
				} else
					representativeList.add(curr);
			}
		}
		// System.out.println("To be removed size:" + toBeRemoved.size());
		return representativeList.size();
	}

	public int[][] generateDistanceMatrix(Vector<Vertex> vList) {
		distanceMatrix = new int[vList.size()][vList.size()];

		for (int a = 0; a < vList.size(); a++)// initialize
		{
			for (int b = 0; b < vList.size(); b++) {
				distanceMatrix[a][b] = 0;
			}
		}

		VertexPair vp;
		int shortestDistance;
		for (int i = 0; i < vList.size(); i++) {
			for (int j = i + 1; j < vList.size(); j++) {
				vp = new VertexPair(vList.get(i), vList.get(j));
				shortestDistance = vp.getShortestDistance();
				distanceMatrix[vList.indexOf(vp.vertex1)][vList.indexOf(vp.vertex2)] = shortestDistance;
				distanceMatrix[vList.indexOf(vp.vertex2)][vList.indexOf(vp.vertex1)] = shortestDistance;
			}
		}
		return distanceMatrix;
	}

	public void displayContainers() {
		vpList = new Vector<VertexPair>();
		int[] kWideGraph = new int[10];
		for (int i = 0; i < kWideGraph.length; i++) {
			kWideGraph[i] = -1;
		}

		VertexPair vp;

		for (int a = 0; a < vList.size(); a++) { // assign vertex pairs
			for (int b = a + 1; b < vList.size(); b++) {
				vp = new VertexPair(vList.get(a), vList.get(b));
				vpList.add(vp);
				int longestWidth = 0;
				System.out.println(">Vertex Pair " + vList.get(a).name + "-" + vList.get(b).name + "\n All Paths:");
				vp.generateVertexDisjointPaths();
				for (int i = 0; i < vp.VertexDisjointContainer.size(); i++) {// for
																				// every
																				// container
																				// of
																				// the
																				// vertex
																				// pair
					int width = vp.VertexDisjointContainer.get(i).size();
					Collections.sort(vp.VertexDisjointContainer.get(i), new descendingWidthComparator());
					int longestLength = vp.VertexDisjointContainer.get(i).firstElement().size();
					longestWidth = Math.max(longestWidth, width);
					System.out.println("\tContainer " + i + " - " + "Width=" + width + " - Length=" + longestLength);

					for (int j = 0; j < vp.VertexDisjointContainer.get(i).size(); j++) // for
																						// every
																						// path
																						// in
																						// the
																						// container
					{
						System.out.print("\t\tPath " + j + "\n\t\t\t");
						for (int k = 0; k < vp.VertexDisjointContainer.get(i).get(j).size(); k++) {
							System.out.print("-" + vp.VertexDisjointContainer.get(i).get(j).get(k).name);
						}
						System.out.println();
					}

				}
				// d-wide for vertexPair
				for (int k = 1; k <= longestWidth; k++) { // 1-wide, 2-wide,
															// 3-wide...
					int minLength = 999;
					for (int m = 0; m < vp.VertexDisjointContainer.size(); m++) // for
																				// each
																				// container
																				// with
																				// k-wide
																				// select
																				// shortest
																				// length
					{
						minLength = Math.min(minLength, vp.VertexDisjointContainer.get(m).size());
					}
					if (minLength != 999) {
						// System.out.println(k + "-wide for vertexpair(" +
						// vp.vertex1.name + "-" + vp.vertex2.name + ")=" +
						// minLength);
						kWideGraph[k] = Math.max(kWideGraph[k], minLength);
					}
				}
			}
		}

		for (int i = 0; i < kWideGraph.length; i++) {
			if (kWideGraph[i] != -1) {
				// System.out.println("D" + i + "(G)=" + kWideGraph[i]);
			}
		}

	}

	public void drawAdjacencyMatrix(Graphics g, Vector<Vertex> vList, int x, int y) {
		int cSize = 20;
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(x, y - 30, vList.size() * cSize + cSize, vList.size() * cSize + cSize);
		g.setColor(Color.black);
		g.drawString("AdjacencyMatrix", x, y - cSize);
		for (int i = 0; i < vList.size(); i++) {
			g.setColor(Color.RED);
			g.drawString(vList.get(i).name, x + cSize + i * cSize, y);
			g.drawString(vList.get(i).name, x, cSize + i * cSize + y);
			g.setColor(Color.black);
			for (int j = 0; j < vList.size(); j++) {
				g.drawString("" + adjacencyMatrix[i][j], x + cSize * (j + 1), y + cSize * (i + 1));
			}
		}
	}

	public void drawDistanceMatrix(Graphics g, Vector<Vertex> vList, int x, int y) {
		int cSize = 20;
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(x, y - 30, vList.size() * cSize + cSize, vList.size() * cSize + cSize);
		g.setColor(Color.black);
		g.drawString("ShortestPathMatrix", x, y - cSize);
		for (int i = 0; i < vList.size(); i++) {
			g.setColor(Color.RED);
			g.drawString(vList.get(i).name, x + cSize + i * cSize, y);
			g.drawString(vList.get(i).name, x, cSize + i * cSize + y);
			g.setColor(Color.black);
			for (int j = 0; j < vList.size(); j++) {
				g.drawString("" + distanceMatrix[i][j], x + cSize * (j + 1), y + cSize * (i + 1));
			}
		}
	}

	public int maximumMatching(Vector<Vertex> vList, Vector<Edge> eList) {
		// Lagyan mo 'to
		return 1;
	}

	// Based on Bron-Kerbosch algorithm
	public void findMaximalClique() {
		Vector<Vertex> R = new Vector<Vertex>();
		Vector<Vertex> P = new Vector<Vertex>();
		Vector<Vertex> X = new Vector<Vertex>();
		P.setSize(vList.size());
		Collections.copy(P, vList);
		BronKerbosch(R, P, X);
	}

	private void BronKerbosch(Vector<Vertex> R, Vector<Vertex> P, Vector<Vertex> X) {
		if (P.size() == 0 && X.size() == 0) {
			// System.out.println("Clique members: ");
			// for(Vertex c : R){
			// System.out.print(c.name + " ");
			// }
			Vector<Vertex> toAdd = new Vector<Vertex>();
			toAdd.setSize(R.size());
			Collections.copy(toAdd, R);
			maximalCliques.add(toAdd);
			// return;
			R.removeAllElements();
		} else {
			// for(int i = 0; i < P.size(); i++){
			// for (Vertex v : P) {
			for (Iterator<Vertex> iterator = P.iterator(); iterator.hasNext();) {
				Vertex v = iterator.next();
				R.addElement(v);
				Vector<Vertex> P_temp = new Vector<Vertex>();
				Vector<Vertex> X_temp_toRemove = new Vector<Vertex>();
				Vector<Vertex> P_temp_toRemove = new Vector<Vertex>();
				P_temp.setSize(P.size());
				Collections.copy(P_temp, P);
				for (Vertex currV_P : P_temp) {
					if (!v.connectedVertices.contains(currV_P)) {
						P_temp_toRemove.add(currV_P);
					}
				}
				for (Vertex currV_X : X) {
					if (!v.connectedVertices.contains(currV_X)) {
						X_temp_toRemove.add(currV_X);
					}
				}
				P_temp.removeAll(P_temp_toRemove);
				X.removeAll(X_temp_toRemove);
				BronKerbosch(R, P_temp, X);
				// P.remove(v);
				X.add(v);
				// try{
				iterator.remove();
				// }catch(Exception e){};
			}
		}
		// return;
	}

	public Vector<Edge> edgeConnectivity() { // Greedy, check other file for
												// counter to this version
		Vector<Edge> origList = new Vector<Edge>();
		Vector<Edge> minCutEdgeSet = new Vector<Edge>();
		Vector<Edge> victimEdgeSet = new Vector<Edge>();
		Vector<Vertex> origVList = new Vector<Vertex>();

		// If G has p nodes and min(G) >= [p/2], then lambda(G) = min(G);
		// For complete graphs, K(G) = lambda(G) = n-1;
		origList.setSize(eList.size());
		origVList.setSize(vList.size());

		Collections.copy(origVList, vList);
		Collections.copy(origList, eList);

		Collections.sort(origVList, new ascendingDegreeComparator());
		Vertex victim = origVList.firstElement();
		for (Edge e : origList) {
			if (!victimEdgeSet.contains(e)) { // Add all edges incident to
												// victim
				if (e.vertex1.name == victim.name || e.vertex2.name == victim.name) {
					victimEdgeSet.add(e);
				}
			}
		}
		while (graphConnectivity(origVList)) { // while original graph is
												// connected
			Edge edgeVictim = victimEdgeSet.elementAt(0);
			victimEdgeSet.remove(edgeVictim);
			minCutEdgeSet.add(edgeVictim);
			for (Vertex v : origVList) {
				if (v.name == edgeVictim.vertex1.name) {
					v.connectedVertices.remove(edgeVictim.vertex2);
				} else if (v.name == edgeVictim.vertex2.name) {
					v.connectedVertices.remove(edgeVictim.vertex1);
				}
			}
		}
		return minCutEdgeSet;
	}

	public Vector<Vertex> vertexConnectivity() {
		Vector<Vertex> origList = new Vector<Vertex>();
		Vector<Vertex> tempList = new Vector<Vertex>();
		Vector<Vertex> toBeRemoved = new Vector<Vertex>();
		Vertex victim;

		origList.setSize(vList.size());
		Collections.copy(origList, vList);

		int maxPossibleRemove = 0;
		while (graphConnectivity(origList)) { // While original graph is
												// connected
			Collections.sort(origList, new ascendingDegreeComparator());
			maxPossibleRemove = origList.firstElement().getDegree();

			for (Vertex v : origList) {
				if (v.getDegree() == maxPossibleRemove) {
					for (Vertex z : v.connectedVertices) {
						if (!tempList.contains(z)) {
							tempList.add(z);
						}
					}
				}
			}

			while (graphConnectivity(origList) && tempList.size() > 0) {
				Collections.sort(tempList, new descendingDegreeComparator());
				victim = tempList.firstElement();
				tempList.removeElementAt(0);
				origList.remove(victim);
				for (Vertex x : origList) {
					x.connectedVertices.remove(victim);
				}
				toBeRemoved.add(victim);
			}
			tempList.removeAllElements();
			// tempList.set
		}
		return toBeRemoved;
	}

	public boolean isGraphConnected(Vector<Vertex> vList) {
		return graphConnectivity(vList);
	}

	private boolean graphConnectivity(Vector<Vertex> vertexList) {
		Vector<Vertex> visitedList = new Vector<Vertex>();
		recurseGraphConnectivity(vertexList.firstElement().connectedVertices, visitedList); // recursive
																							// function
		return (visitedList.size() == vertexList.size());
	}

	private void recurseGraphConnectivity(Vector<Vertex> vertexList, Vector<Vertex> visitedList) {
		for (Vertex v : vertexList) {
			if (!visitedList.contains(v)) {
				visitedList.add(v);
				recurseGraphConnectivity(v.connectedVertices, visitedList);
			}
		}
	}

	private class ascendingDegreeComparator implements Comparator {

		public int compare(Object v1, Object v2) {

			if (((Vertex) v1).getDegree() > ((Vertex) v2).getDegree()) {
				return 1;
			} else if (((Vertex) v2).getDegree() > ((Vertex) v1).getDegree()) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	private class descendingDegreeComparator implements Comparator {

		public int compare(Object v1, Object v2) {

			if (((Vertex) v1).getDegree() > ((Vertex) v2).getDegree()) {
				return -1;
			} else if (((Vertex) v2).getDegree() > ((Vertex) v1).getDegree()) {
				return 1;
			} else {
				return 0;
			}
		}
	}

	private class descendingWidthComparator implements Comparator {

		public int compare(Object v1, Object v2) {

			if (((Vector<Vertex>) v1).size() > (((Vector<Vertex>) v2).size())) {
				return -1;
			} else if (((Vector<Vertex>) v1).size() < (((Vector<Vertex>) v2).size())) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
